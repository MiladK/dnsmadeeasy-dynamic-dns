#!/usr/bin/env node

const axios = require('axios')
const dotenv = require('dotenv')

const handleError = err => {
  console.error(err)

  process.exit(1)
}

const returnResponse = res => res.data

const readConfig = () => {
  return new Promise((resolve, reject) => {
    const result = dotenv.config()

    if (result.error) {
      reject(result.error)
    } else {
      resolve(result.parsed)
    }
  })
}

const update = () => {
  return Promise.all([
    readConfig(),
    axios.get('https://myip.dnsomatic.com/').then(returnResponse)
  ])
    .then(([config, ip]) => {
      const query = `username=${config.username}&password=${config.password}&id=${config.ids}&ip=${ip}`
      const url = `https://cp.dnsmadeeasy.com/servlet/updateip?${query}`

      return axios.get(url).then(returnResponse)
    })
}

update()
  .then(console.log)
  .catch(handleError)
