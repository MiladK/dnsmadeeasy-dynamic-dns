# DNS MADE EASY - Dynamic DNS Updater

This is an implementation of [DNSMadeEasy Dynamic DNS](https://dnsmadeeasy.com/technology/dynamic-dns/) in Node.js.

## Installation
- Run:
```shell script
npm i -g dnsmadeeasy-dynamic-dns
cp .env.example .env
```
- Fill the information in the .env file.
    - username: The username you use to log into DNS Made Easy.
    - password: The password you use to log into DNS Made Easy (or per record password if configured).
    - ids: The id or the ids (comma-separated) of the records that you want to update.
- Run:
```shell script
./dns.js
```
